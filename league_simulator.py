#!/usr/bin/env python3

import pandas as pd
import numpy as np
import random
import itertools
from datetime import datetime
import os
import argparse
import time


class LeagueSimulator:
    def __init__(self):
        self.matches = list()

        self.national_structure = pd.read_csv('national_structure.csv')
        self.league_structure = pd.read_csv('league_structure.csv')
        self.teams = self.team_sheet()

        self.leagues = {'Tier 3': 'Tier 3', "Tier 2": "Tier 2", "Tier 1": "Tier 1"} | {
            x: 'Country' for x in set(self.teams['Country'])} | {
            x: 'L3' for x in set(self.teams['L3'])} | {
            x: 'L4' for x in set(self.teams['L4']) if x is not np.nan} | {
            x: 'L5' for x in set(self.teams['L5']) if x is not np.nan}
        self.league_structure.index = self.league_structure['League']
        self.round = 0
        self.league_schedule = {'L5': 7, "L4": 6, "L3": 5, "Country": 4, 'Tier 3': 3, "Tier 2": 2, "Tier 1": 1}
        self.reverse_schedule = {7: 'L5', 6: 'L4', 5: 'L3', 4: 'Country', 3: 'Tier 3', 2: 'Tier 2', 1: 'Tier 1'}
        self.debug = False

    def team_sheet(self):
        teams = []
        for row in self.national_structure.iterrows():
            i = row[1]
            for value in range(i['Total teams']):
                level = 7
                current_league = i['Level 5']
                if i['Level 5'] is np.nan:
                    level = 6
                    current_league = i['Level 4']
                    if i['Level 4'] is np.nan:
                        level = 5
                        current_league = i['Level 3']
                teams.append([i['City'], i['State/Province'], i['Country'], i['Level 3'], i['Level 4'], i['Level 5'],
                              f"{i['City']} {i['State/Province']} {value}", level, current_league, 0, 0, 0, 0, 0])
        teams = pd.DataFrame(teams)
        teams.columns = ['City', 'State', 'Country', 'L3', 'L4', 'L5', 'Team number', 'Level', "League", 'Offense',
                         'Defense', 'Wins', 'Losses', 'Draws']
        teams.index = teams['Team number']
        teams['Country'] = [x if x in ['USA', 'Canada'] else 'Caribbean' for x in teams['Country']]
        return teams

    def football_match(self, alpha, beta):
        team_1 = self.teams.loc[alpha]
        team_2 = self.teams.loc[beta]
        meters = 0
        team_1_score = 0
        team_2_score = 0
        for x in range(0, 90):
            rand = random.randint(0, 1)
            if rand > 0:
                meters += rand * 10 * (random.random() - .5) * (1 + team_1['Offense']) - team_2['Defense']
            if rand < 0:
                meters -= rand * 10 * (random.random() - .5) * (1 + team_2['Offense']) - team_1['Defense']
            if meters > 50:
                team_1_score += 1
            if meters < -50:
                team_2_score += 1
            if rand > 0.95:
                team_1_score += 1
            if rand <= 0.95:
                team_2_score += 1
        if team_1_score > team_2_score:
            self.teams.loc[alpha, 'Wins'] += 1
            self.teams.loc[beta, 'Losses'] += 1
            return team_1['Team number']
        elif team_2_score > team_1_score:
            self.teams.loc[beta, 'Wins'] += 1
            self.teams.loc[alpha, 'Losses'] += 1
            return team_2['Team number']
        else:
            self.teams.loc[alpha, 'Draws'] += 1
            self.teams.loc[beta, 'Draws'] += 1
            return 0

    def league_tournament(self, league):
        teams = self.teams[self.teams['League'] == league]
        names = teams.index.to_list()
        matches = list(itertools.combinations(names, 2))
        for match in matches:
            self.football_match(match[0], match[1])

    def run_all_leagues(self):
        for league in self.leagues.keys():
            self.league_tournament(league)

    def get_league(self):
        league_list = []
        for idx in self.teams.index:
            league = self.teams.League[idx]
            if league in self.teams.columns:
                league_list.append(self.teams[league][idx])
            else:
                league_list.append(self.reverse_schedule[self.teams.Level[idx]])

        self.teams['League'] = league_list

    def promotion_relegation(self):
        self.teams['Wins'] = 0
        self.teams['Losses'] = 0
        self.run_all_leagues()
        self.teams['Net wins'] = self.teams['Wins'] - self.teams['Losses']

        st = self.teams

        for league, level in self.leagues.items():
            spec = self.league_structure.loc[league]
            temp = st[st['League'] == league]
            if temp.shape[0] and spec['Promoted'] > 0:
                promoted = temp.sort_values('Net wins', ascending=False).head(int(spec['Promoted']))
                for team in promoted.index:
                    self.teams.loc[team, "Level"] -= 1

            self.teams['League'] = [self.reverse_schedule[level] for level in self.teams['Level']]
            self.get_league()

            if 1 < spec['Level'] < max(self.reverse_schedule.keys()):
                if temp.shape[0] > spec['Teams']:
                    over = temp.shape[0] - int(spec['Teams'])
                    relegated = self.teams[self.teams['League'] == league].sort_values('Wins').head(over)
                    for team in relegated.index:
                        self.teams.loc[team, "Level"] += 1
                if temp.shape[0] == spec['Teams']:
                    relegated = self.teams[self.teams['League'] == league].sort_values('Wins').head(int(spec['Teams']))
                    for team in relegated.index:
                        self.teams.loc[team, "Level"] += 1

        relegated = self.teams[self.teams['League'] == 'Tier 1'].sort_values('Wins').head(-32)
        for team in relegated.index:
            self.teams.loc[team, "Level"] = 2
            self.teams.loc[team, "League"] = 'Tier 2'

        self.teams['League'] = [self.reverse_schedule[level] for level in self.teams['Level']]
        self.get_league()
        self.teams[f"Round {self.round}"] = self.teams['Level']
        self.round += 1


def main():
    parser = argparse.ArgumentParser(
        prog="league simulator",
        description="simulate a promotion and relegation league over many seasons"
    )
    now = datetime.now()
    csv_dest = f"results/results-{now.year}-{now.month}-{now.day}-"\
               f"{now.hour}-{now.minute}-{now.second}.csv"
    parser.add_argument('--csv', help="save results to csv file", action='store_true')
    parser.add_argument('--dest', help="change destination of csv results", default=csv_dest)
    parser.add_argument('--iterations', help='Iterations, default 30', default=30, type=int)
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    tic = time.perf_counter()

    simulation = LeagueSimulator()
    simulation.debug = args.debug
    for iteration in range(args.iterations):
        simulation.promotion_relegation()
        if simulation.debug:
            print(f'iteration number {iteration}')

    if args.csv or args.dest != csv_dest:
        if not os.path.isdir('results'):
            os.mkdir('results')
        simulation.teams.to_csv(args.dest)
        print(f'Results saved to {args.dest}!')
    if args.debug:
        toc = time.perf_counter()
        print(f"Code finished in {time.strftime('%H:%M:%S', time.gmtime(toc - tic))}")


if __name__ == "__main__":
    main()
