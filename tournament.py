#!/usr/bin/env python
# coding: utf-8

import random
import numpy as np
import uuid
import pandas as pd
from datetime import datetime


class Tournament:
    def __init__(self):
        self.games_played = 0
        self.rounds = random.randint(2, 10)
        self.team_count = 2 ** self.rounds
        self.teams = dict()
        for _ in range(128):
            team = self.generate_team()
            self.teams[team[0]] = team[1]

    @staticmethod
    def generate_team():
        stats = ['offense',
                 'defense',
                 'possession',
                 'passing',
                 'shooting',
                 'goalie',
                 'dribble',
                 'side tackle',
                 'blocking']
        general_quality = random.randint(1, 10)
        statistics = dict()
        for stat in stats:
            statistics[stat] = general_quality * random.randint(1, 10)
        statistics['name'] = uuid.uuid1().int * random.random()
        statistics['wins'] = 0
        statistics['losses'] = 0
        statistics['draws'] = 0
        statistics['total score'] = 0
        statistics['GF'] = 0
        statistics['GA'] = 0
        statistics['Final tournament victories'] = 0
        return statistics['name'], statistics

    def game_simulation(self, alpha, beta, overtime=False):
        self.games_played += 1
        alpha_name = alpha['name']
        beta_name = beta['name']
        teams = {alpha_name: alpha,
                 beta_name: beta}
        score = {alpha_name: 0,
                 beta_name: 0}
        meter = 0
        # print(meter)
        possession = alpha_name
        defense = beta_name
        for minute in range(0, 90):
            if abs(meter) > 100:  # shoot for the goal
                success = teams[possession]['shooting'] * teams[possession]['offense'] * random.randint(1, 10) - \
                          teams[defense]['goalie'] * teams[defense]['defense'] * random.randint(1, 10)
                # print('shoot: %s, meter %s, %s' % (success, meter, minute))
                if success > 0:
                    score[alpha_name] += 1
                    # print('Goal at %s for %s' % (minute, possession))
                    possession, defense = defense, possession
                    meter = 0
            else:
                if random.randint(1, 10) % 2:  # pass
                    success = teams[possession]['passing'] * teams[possession]['offense'] * random.randint(1, 10) - \
                              teams[defense]['blocking'] * teams[defense]['defense'] * random.randint(1, 10)
                    # print('pass: %s, meter %s, %s' % (success, meter, minute))
                    if success > 0:
                        possession, defense = defense, possession
                    else:
                        meter += random.randint(1, 30)
                else:  # dribble
                    success = teams[possession]['dribble'] * teams[possession]['offense'] * random.randint(1, 10) - \
                              teams[defense]['side tackle'] * teams[defense]['defense'] * random.randint(1, 10)
                    # print('dribble: %s, meter %s, %s' % (success, meter, minute))
                    if success > 0:
                        possession, defense = defense, possession
                    else:
                        meter += random.randint(1, 10)
        if overtime:
            if list(score.values())[0] - list(score.values())[1] == 0:
                for _ in range(1, 12):
                    success = teams[possession]['shooting'] * teams[possession]['offense'] * random.randint(1, 10) - \
                              teams[defense]['goalie'] * teams[defense]['defense'] * random.randint(1, 10)
                    # print('shoot: %s, meter %s, %s' % (success, meter, minute))
                    if success > 0:
                        score[alpha_name] += 1
                        # print('Goal at %s for %s' % (minute, possession))
                    possession, defense = defense, possession
            while list(score.values())[0] - list(score.values())[1] == 0:
                success = teams[possession]['shooting'] * teams[possession]['offense'] * random.randint(1, 10) - \
                          teams[defense]['goalie'] * teams[defense]['defense'] * random.randint(1, 10)
                # print('shoot: %s, meter %s, %s' % (success, meter, minute))
                if success > 0:
                    score[alpha_name] += 1
                    # print('Goal at %s for %s' % (minute, possession))
                possession, defense = defense, possession
        return score

    def calculate_rank(self):
        for key, value in self.teams.items():
            self.teams[key]['total score'] = value['wins'] * 3 - value['losses'] * 3 + value['draws']

    def swiss_round(self, home, away, overtime=False):
        result = self.game_simulation(self.teams[home], self.teams[away], overtime)
        self.teams[home]['GF'] = result[home]
        self.teams[away]['GF'] = result[home]

        self.teams[away]['GA'] = result[home]
        self.teams[home]['GA'] = result[away]
        if list(result.values())[0] - list(result.values())[1] == 0:
            self.teams[home]['draws'] += 1
            self.teams[away]['draws'] += 1
        else:
            winner = max(result, key=result.get)
            loser = min(result, key=result.get)
            self.teams[winner]['wins'] += 1
            self.teams[loser]['losses'] += 1
        return result

    def swiss_style(self):
        # initial round
        home_teams = ([x for x in self.teams.keys()][::2])
        away_teams = ([x for x in self.teams.keys()][1::2])
        for home, away in list(zip(home_teams, away_teams)):
            result = self.game_simulation(self.teams[home], self.teams[away])
            if list(result.values())[0] - list(result.values())[1] == 0:
                self.teams[home]['draws'] += 1
                self.teams[away]['draws'] += 1
            else:
                winner = max(result, key=result.get)
                lower = min(result, key=result.get)
                self.teams[winner]['wins'] += 1
                self.teams[lower]['losses'] += 1

        # subsequent rounds
        for game in range(0, int(np.log(len(self.teams)) / np.log(2))):
            total_games = 0
            self.calculate_rank()
            standings = {}
            scores = set([y['total score'] for x, y in self.teams.items()])
            for score in scores:
                standings[score] = [x for x, y in self.teams.items() if y['total score'] == score]
            print(scores)
            for score in scores:
                valid_teams = {key: value for key, value in self.teams.items() if value['total score'] == score}
                home_teams = ([x for x in valid_teams.keys()][::2])
                away_teams = ([x for x in valid_teams.keys()][1::2])
                for home, away in list(zip(home_teams, away_teams)):
                    self.swiss_round(home, away)
                    total_games += 1
            print(game, total_games)  # , Counter([y['total score'] for x,y in teams.items()]))
        self.calculate_rank()

    def pyramid_style(self):
        final = pd.DataFrame(self.teams.values())
        final.index = self.teams.keys()
        final.sort_values('total score').tail(32)
        valid_teams = list(final.index)
        while len(valid_teams) > 1:
            home_teams = valid_teams[::2]
            away_teams = valid_teams[1::2]
            for home, away in list(zip(home_teams, away_teams)):
                result = self.swiss_round(home, away, overtime=True)
                self.teams[max(result, key=result.get)]['Final tournament victories'] += 1
                valid_teams.remove(min(result, key=result.get))
            print(valid_teams)

    def export_csv(self):
        final = pd.DataFrame(self.teams.values())
        final.index = self.teams.keys()
        now = datetime.now()
        final.to_csv('results/tournament_results_%s_%s_%s_%s_%s.csv' % (now.year, now.month, now.day, now.hour, now.minute))


def main():
    tourney = Tournament()
    tourney.swiss_style()
    tourney.pyramid_style()
    tourney.export_csv()


if __name__ == "__main__":
    main()
